var projector, mouse = {
    x: 0,
    y: 0
  },
  INTERSECTED;

var ray = new THREE.Raycaster();
var mousee = new THREE.Vector2();
var objects = []
var latestMouseProjection;
var hoveredObj;
var tooltipDisplayTimeout;


//SCENE
var scene = new THREE.Scene();
const loader = new THREE.TextureLoader();
const bgTexture = loader.load('assets/imgs/gradient-bg.jpg');
scene.background = bgTexture;

//RENDERER
var renderer = new THREE.WebGLRenderer();
renderer.setPixelRatio(window.devicePixelRatio)
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement);

//CAMERA
var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
camera.position.z = 35;

// CONTROLS
var controls = new THREE.OrbitControls(camera, renderer.domElement)
controls.enableDamping = false;
controls.dampingFactor = 1.80;
controls.enableZoom = true;
controls.screenSpacePanning = true;
controls.center.set(0, 0, 0);
controls.keys = {
  LEFT: 37, //left arrow
  UP: 38, // up arrow
  RIGHT: 39, // right arrow
  BOTTOM: 40 // down arrow
}



//LIGHTS
var frontLight = new THREE.DirectionalLight(0xffffff, 0.2)
var backLight = new THREE.DirectionalLight(0xffffff, 0.3)
var ambientLight = new THREE.AmbientLight(0xffffff, 0.8);

ambientLight.position.set(100, 0, 100)
frontLight.position.set(-10, 10, 100).normalize()
backLight.position.set(100, 0, -100).normalize()

scene.add(ambientLight);
scene.add(frontLight)
scene.add(backLight)

var system = 'urinarySystem';
var model = {}


// DigestiveSystem MODEL/MATERIAL LOADING!
var mtlLoader2 = new THREE.MTLLoader();
mtlLoader2.setTexturePath("models/obj digestive/");
mtlLoader2.setPath("models/obj digestive/");
mtlLoader2.load("full final.mtl", (materials) => {
  document.getElementById('loader').style.display = 'block';
  materials.preload();
  //     // OBJECT LOADER
  var objLoader = new THREE.OBJLoader();
  objLoader.setMaterials(materials);
  objLoader.setPath("models/obj digestive/");
  objLoader.load("full final.obj", (object) => {
    object.position.y -= 3.5;
    object.position.x = 0;
    object.children[0].name = 'الشرج'
    object.children[1].name = 'الأمعاء الغليظة'
    object.children[2].name = 'المرارة'
    object.children[3].name = 'المستقيم'
    object.children[4].name = 'المرارة'
    object.children[5].name = 'المرارة'
    object.children[6].name = 'الأمعاء الدقيقة'
    object.children[7].name = 'المعدة'
    object.children[10].name = 'البنكرياس'
    object.children[11].name = 'الكبد'
    object.children[12].name = 'المريء'
    model.digestive = object
    openFolders()
  });
});
// Kidny MODEL/MATERIAL LOADING!
var mtlLoader3 = new THREE.MTLLoader();
mtlLoader3.setTexturePath("models/anatomy/");
mtlLoader3.setPath("models/anatomy/");
mtlLoader3.load("layer1.mtl", (materials) => {
  document.getElementById('loader').style.display = 'block';
  materials.preload();
  //     // OBJECT LOADER
  var objLoader = new THREE.OBJLoader();
  objLoader.setMaterials(materials);
  objLoader.setPath("models/anatomy/");
  objLoader.load("layer1.obj", (object) => {
    object.position.y -= 6;
    object.children[0].name = 'اضغط على الكلية لتشريحها'
    object.children[0].memberName = 'الكليتان'
    object.children[1].name = 'اضغط على الكلية لتشريحها'
    object.children[1].memberName = 'الكليتان'
    object.children[2].name = 'اضغط على الكلية لتشريحها'
    object.children[2].memberName = 'الكليتان'
    object.children[3].name = 'اضغط على الكلية لتشريحها'
    object.children[3].memberName = 'الكليتان'
    model.kidny = object
    openFolders()
  });
  //
});
// Kidny2 MODEL/MATERIAL LOADING!

var mtlLoader4 = new THREE.MTLLoader();
mtlLoader4.setTexturePath("models/anatomy/");
mtlLoader4.setPath("models/anatomy/");
mtlLoader4.load("layer2.mtl", (materials) => {
  document.getElementById('loader').style.display = 'block';
  materials.preload();
  //     // OBJECT LOADER
  var objLoader = new THREE.OBJLoader();
  objLoader.setMaterials(materials);
  objLoader.setPath("models/anatomy/");
  objLoader.load("layer2.obj", (object) => {
    object.position.y -= 6;
    object.position.x = -3;
    object.children[1].name = 'الغلاف'
    object.children[1].memberName = 'الكليتان'
    object.children[6].name = 'الغلاف'
    object.children[6].memberName = 'الكليتان'
    object.children[0].name = 'وريد كلوي'
    object.children[0].memberName = 'الكليتان'
    object.children[4].name = 'شريان كلوي'
    object.children[4].memberName = 'الكليتان'
    object.children[2].name = 'القشرة'
    object.children[2].memberName = 'الكليتان'
    object.children[3].name = 'القشرة'
    object.children[3].memberName = 'الكليتان'
    object.children[5].name = 'القشرة'
    object.children[5].memberName = 'الكليتان'
    model.kidny2 = object
  });
});

// Kidny3 MODEL/MATERIAL LOADING!

var mtlLoader5 = new THREE.MTLLoader();
mtlLoader5.setTexturePath("models/anatomy/");
mtlLoader5.setPath("models/anatomy/");
mtlLoader5.load("layer3.mtl", (materials) => {
  document.getElementById('loader').style.display = 'block';
  materials.preload();
  //     // OBJECT LOADER
  var objLoader = new THREE.OBJLoader();
  objLoader.setMaterials(materials);
  objLoader.setPath("models/anatomy/");
  objLoader.load("layer3.obj", (object) => {
    object.position.y -= 6;
    object.position.x = -4;
    object.children[1].name = 'الغلاف'
    object.children[1].memberName = 'الكليتان'
    object.children[6].name = 'الغلاف'
    object.children[6].memberName = 'الكليتان'
    object.children[0].name = 'وريد كلوي'
    object.children[0].memberName = 'الكليتان'
    object.children[4].name = 'شريان كلوي'
    object.children[4].memberName = 'الكليتان'
    object.children[2].name = 'النخاع'
    object.children[2].memberName = 'الكليتان'
    object.children[3].name = 'الاهرامات'
    object.children[3].memberName = 'الكليتان'
    object.children[5].name = 'القشرة'
    object.children[5].memberName = 'الكليتان'
    model.kidny3 = object;
    document.getElementById('loader').style.display = 'none';
  });
  //
});
// urinarySystem MODEL/MATERIAL LOADING!
var mtlLoader = new THREE.MTLLoader();
mtlLoader.setTexturePath("models/urinarySystem3D/");
mtlLoader.setPath("models/urinarySystem3D/");
mtlLoader.load("full-textuer-uninery.mtl", (materials) => {
  document.getElementById('loader').style.display = 'block';
  materials.preload();
  //     // OBJECT LOADER
  var objLoader = new THREE.OBJLoader();
  objLoader.setMaterials(materials);
  objLoader.setPath("models/urinarySystem3D/");
  objLoader.load("full-textuer-uninery.obj", (object) => {
    object.position.y = 0;
    object.position.x = 0;
    object.traverse((children) => {
      children.callback = function() {
        return {
          name: this.name,
          member: this.memberName
        }
      }
      objects.push(children)
    });
    getSystemName(system)
    object.children[0].name = 'الأورطي'
    object.children[1].name = 'الوريد الأجوف السفلي'
    object.children[3].name = 'الغدة الكظرية'
    object.children[4].name = 'الغدة الكظرية'
    object.children[5].name = 'وريد كلوي'
    object.children[6].name = 'وريد كلوي'
    object.children[7].name = 'الكلية اليسرى'
    object.children[7].memberName = 'الكليتان'
    object.children[8].name = 'الكلية اليسرى'
    object.children[8].memberName = 'الكليتان'
    object.children[10].name = 'الشريان الكلوي'
    object.children[11].name = 'الحالب الأيمن'
    object.children[11].memberName = 'الحالبان'
    object.children[12].name = 'الحالب الأيسر'
    object.children[12].memberName = 'الحالبان'
    object.children[13].name = 'المثانة'
    object.children[13].memberName = 'المثانة'
    object.children[14].name = 'الكلية اليمنى'
    object.children[14].memberName = 'الكليتان'
    model.urinarySystem = object;
    openFolders()
    scene.add(model.urinarySystem);
  });
});

var controlers = new function() {
  this.digestiveSystem = false;
  this.urinarySystem = false;
  this.fullKidny = false;
  this.rotate = false;
  this.unRotate = false;
  this.frontLight = 0.2;
  this.backLight = 0.3;
  this.ambientLight = 0.8
  this.reset = false
  this.fontSize = 18
};


var gui = new dat.GUI();
const rotaion = gui.addFolder('الدوران')
const lights = gui.addFolder('الاضاءة')
const urinarySystem = gui.addFolder('الجهاز البولي')
const digestiveSystem = gui.addFolder('الجهاز الهضمي')
const fontSize = gui.addFolder('حجم الخط')
gui.add(controlers, 'reset').name('اعادة ضبط الاعدادات').listen().onChange(e => {
  controls.reset();
  controlers.reset = false
})
rotaion.add(controlers, 'rotate').name('تشغيل الدوران').listen().onChange(e => {
  controlers.rotate = true
  controlers.unRotate = false
  controls.autoRotate = true;
  controlers.reset = false

})
rotaion.add(controlers, 'unRotate').name('ايقاف التشغيل').listen().onChange(e => {
  controlers.unRotate = true
  controlers.rotate = false
  controls.autoRotate = false;
  controlers.reset = false
})
fontSize.add(controlers, 'fontSize', 15, 23).name('حجم النص').listen().onChange(value => {
  var elements = document.getElementsByClassName('text');

  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.style.fontSize = value + 'px';
  }
})
lights.add(controlers, 'ambientLight', 0, 0.8).name('الاضاءة العامة  ').listen().onChange(value => {
  ambientLight.intensity = value
  controlers.reset = false

})
lights.add(controlers, 'frontLight', 0, 0.15).name('الاضاءة الامامية  ').listen().onChange(value => {
  frontLight.intensity = value
  controlers.reset = false


})
lights.add(controlers, 'backLight', 0, 0.15).name('الاضاءة الخلفية  ').listen().onChange(value => {
  backLight.intensity = value
  controlers.reset = false

})
digestiveSystem.add(controlers, 'digestiveSystem').name('الجهاز الهضمي كاملاَ').listen().onChange(e => {
  controlers.digestiveSystem = true
  controlers.urinarySystem = false
  controlers.fullKidny = false
  controlers.reset = false
  controlers.fontSize = 18
  document.getElementById("h1").innerHTML = ''
  document.getElementById("list").innerHTML = ''
  document.getElementById('list').style.background = 'none'
  scene.remove(scene.children[3]);
  objects.length = 0
  controls.reset();
  camera.position.z = 10;
  ambientLight.intensity = 0.72
  frontLight.intensity = 0.06
  backLight.intensity = 0.06
  camera.position.y = 3;
  system = 'digestiveSystem';
  getSystemName(system)
  model.digestive.traverse((children) => {
    children.callback = function() {
      return {
        name: this.name,
        member: this.name
      }
    }
    objects.push(children)
  });
  scene.add(model.digestive);
});
urinarySystem.add(controlers, 'urinarySystem').name('الجهاز البولي كاملاَ').listen().onChange(e => {
  controlers.urinarySystem = true
  controlers.digestiveSystem = false
  controlers.fullKidny = false
  controlers.reset = false
  controlers.fontSize = 18
  document.getElementById("h1").innerHTML = ''
  document.getElementById("list").innerHTML = ''
  document.getElementById('list').style.background = 'none'
  scene.remove(scene.children[3]);
  objects.length = 0
  controls.reset();
  model.urinarySystem.traverse((children) => {
    children.callback = function() {
      return {
        name: this.name,
        member: this.memberName
      }
    }
    objects.push(children)
  });
  system = 'urinarySystem';
  getSystemName(system)
  scene.add(model.urinarySystem)

});


urinarySystem.add(controlers, 'fullKidny').name('الكلية').listen().onChange(e => {
  controlers.fullKidny = true
  controlers.digestiveSystem = false
  controlers.urinarySystem = false
  controlers.reset = false
  controlers.fontSize = 18
  document.getElementById("h1").innerHTML = ''
  document.getElementById("list").innerHTML = ''
  document.getElementById('list').style.background = 'none'
  scene.remove(scene.children[3]);
  objects.length = 0
  camera.position.z = 15;
  system = 'urinarySystem';
  getSystemName(system, 'الكليتان')
  model.kidny.traverse((children) => {
    children.callback = function() {
      return {
        name: this.name,
        member: this.memberName
      }
    }
    objects.push(children)
  });
  scene.add(model.kidny)
});

var counter = 0;

function anatomy() {
  counter++
  hideTooltip()
  if (counter === 1) {
    document.getElementById("list").innerHTML = ''
    document.getElementById('list').style.background = 'none'
    scene.remove(scene.children[3]);
    objects.length = 0
    camera.position.z = 20;
    model.kidny2.traverse((children) => {
      children.callback = function() {
        return {
          name: this.name,
          member: this.memberName
        }
      }
      objects.push(children)
    });
    system = 'urinarySystem';
    getSystemName(system, 'الكليتان')
    scene.add(model.kidny2)

  } else if (counter === 2) {
    document.getElementById("list").innerHTML = ''
    document.getElementById('list').style.background = 'none'
    scene.remove(scene.children[3]);
    objects.length = 0
    camera.position.z = 20;
    model.kidny3.traverse((children) => {
      children.callback = function() {
        return {
          name: this.name,
          member: this.memberName
        }
      }
      objects.push(children)
    });
    system = 'urinarySystem';
    getSystemName(system, 'الكليتان')
    scene.add(model.kidny3)
  } else {
    counter = 0
  }
}

function showTooltip() {
  var divElement = document.getElementById('tooltip');
  controlers.fontSize = 18
  if (divElement && latestMouseProjection) {
    divElement.style.display = "block";
    divElement.style.opacity = 0.0;
    var canvasHalfWidth = renderer.domElement.offsetWidth / 2;
    var canvasHalfHeight = renderer.domElement.offsetHeight / 2;
    var tooltipPosition = latestMouseProjection.clone().project(camera);
    tooltipPosition.x = (tooltipPosition.x * canvasHalfWidth) + canvasHalfWidth + renderer.domElement.offsetLeft;
    tooltipPosition.y = -(tooltipPosition.y * canvasHalfHeight) + canvasHalfHeight + renderer.domElement.offsetTop;
    var left = tooltipPosition.x - 7
    var top = tooltipPosition.y - 5
    divElement.style.left = left + "px"
    divElement.style.top = top + "px"
    if (hoveredObj.name === 'اضغط على الكلية لتشريحها') {
      divElement.style.background = 'blue'
      divElement.style.cursor = "pointer"
      divElement.addEventListener("click", anatomy, false);
    } else {
      divElement.style.background = '#a0c020'
    }
    divElement.innerHTML = hoveredObj.name;

    setTimeout(function() {
      divElement.style.opacity = 1.0
    }, 25);
  }
  document.getElementById('list').classList.remove('animated')
  document.getElementById('list').classList.remove('fadeInRight')
  getSystemName(system, hoveredObj.member)
}
// This will immediately hide tooltip.
function hideTooltip() {
  var divElement = document.getElementById('tooltip')
  if (divElement) {
    divElement.style.display = "none";
  }
}

// Following two functions will convert mouse coordinates
// from screen to three.js system (where [0,0] is in the middle of the screen)
function updateMouseCoords(event, coordsObj) {
  coordsObj.x = ((event.clientX - renderer.domElement.offsetLeft + 0.5) / window.innerWidth) * 2 - 1;
  coordsObj.y = -((event.clientY - renderer.domElement.offsetTop + 0.5) / window.innerHeight) * 2 + 1;
}

function onDocumentMouseDown2() {
  // event.preventDefault();
  mousee.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
  mousee.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1;
  ray.setFromCamera(mousee, camera);
  var intersects = ray.intersectObjects(objects);
  if (intersects.length > 0) {
    latestMouseProjection = intersects[0].point;
    hoveredObj = intersects[0].object.callback();
  }
  if (tooltipDisplayTimeout || !latestMouseProjection) {
    clearTimeout(tooltipDisplayTimeout);
    tooltipDisplayTimeout = undefined;
    hideTooltip();
  }
  if (!tooltipDisplayTimeout && latestMouseProjection) {
    tooltipDisplayTimeout = setTimeout(function() {
      tooltipDisplayTimeout = undefined;
      showTooltip();
    }, 330);
  }
}

function onMouseMove(event) {
  updateMouseCoords(event, mouse);

  latestMouseProjection = undefined;
  hoveredObj = undefined;
  onDocumentMouseDown2();
}

// this function to make the scene responsive with the window size
function onResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

function openFolders() {
  var folderGui = document.getElementsByClassName('closed')
  for (var i = 0; i < folderGui.length; i++) {
    folderGui[i].classList.remove('closed')
  }
}

const animate = () => {
  requestAnimationFrame(animate);
  controls.update()
  openFolders()
  renderer.render(scene, camera);

};

window.addEventListener('mousemove', onMouseMove, false);
window.addEventListener('resize', onResize, false);
animate();
