// var obj = { urinarySystem : '', digestive: ''}
function generateUrinary(cb) {
  var mtlLoader = new THREE.MTLLoader();
  mtlLoader.setTexturePath("models/urinarySystem3D/");
  mtlLoader.setPath("models/urinarySystem3D/");
  mtlLoader.load("full-textuer-uninery.mtl", (materials) => {

    materials.preload();
    //     // OBJECT LOADER
    var objLoader = new THREE.OBJLoader();
    objLoader.setMaterials(materials);
    objLoader.setPath("models/urinarySystem3D/");
    objLoader.load("full-textuer-uninery.obj", (object) => {
      object.position.y = 0;
      object.position.x = 0;
      object.traverse((children) => {
        children.callback = function() {
          return {
            name: this.name,
            member: this.memberName
          }
        }
        // objects.push(children)
      });
      object.children[0].name = 'الأورطي'
      object.children[1].name = 'الوريد الأجوف السفلي'
      object.children[3].name = 'الغدة الكظرية'
      object.children[4].name = 'الغدة الكظرية'
      object.children[5].name = 'وريد كلوي'
      object.children[6].name = 'وريد كلوي'
      object.children[7].name = 'الكلية اليسرى'
      object.children[7].memberName = 'الكليتان'
      object.children[8].name = 'الكلية اليسرى'
      object.children[8].memberName = 'الكليتان'
      object.children[10].name = 'الشريان الكلوي'
      object.children[11].name = 'الحالب الأيمن'
      object.children[11].memberName = 'الحالبان'
      object.children[12].name = 'الحالب الأيسر'
      object.children[12].memberName = 'الحالبان'
      object.children[13].name = 'المثانة'
      object.children[13].memberName = 'المثانة'
      object.children[14].name = 'الكلية اليمنى'
      object.children[14].memberName = 'الكليتان'
      console.log(object);
      console.log(Object.keys(object).length, 'length');
      // return object
      // request(object)
      cb(object)
    });
  });
}

function generateDigistive(cb) {
  // MODEL/MATERIAL LOADING!
  var mtlLoader = new THREE.MTLLoader();
  mtlLoader.setTexturePath("models/obj digestive/");
  mtlLoader.setPath("models/obj digestive/");
  mtlLoader.load("full final.mtl", (materials) => {
    // document.getElementById('loader').style.display = 'block';

    materials.preload();
    //     // OBJECT LOADER
    var objLoader = new THREE.OBJLoader();
    objLoader.setMaterials(materials);
    objLoader.setPath("models/obj digestive/");
    objLoader.load("full final.obj", (object) => {
      object.position.y -= 3.5;
      camera.position.y = 3;
      camera.position.z = 8;
      object.position.x = 0;
      object.traverse((children) => {
        children.callback = function() {
          return {
            name: this.name,
            member: this.name
          }
        }
        // objects.push(children)
      });
      // document.getElementById('loader').style.display = 'none';
      // system = 'digestiveSystem';
      // getSystemName(system)
      object.children[0].name = 'الشرج'
      object.children[1].name = 'الأمعاء الغليظة'
      object.children[2].name = 'المرارة'
      object.children[3].name = 'المستقيم'
      object.children[4].name = 'المرارة'
      object.children[5].name = 'المرارة'
      object.children[6].name = 'الأمعاء الدقيقة'
      object.children[7].name = 'المعدة'
      object.children[10].name = 'البنكرياس'
      object.children[11].name = 'الكبد'
      object.children[12].name = 'المريء'
      // object.children[8].name = 'الأمعاء الغليظة'
      // object.children[9].name = 'الأمعاء الغليظة'
      // object.children[10].name = 'الأمعاء الدقيقة'
      // scene.add(object);
      // openFolders()
      // return object
      cb(object)
    });
  });
}

function generateKidny() {

}



// async function generateModel() {
//   let promise = new Promise((resolve, reject) => {
//     generateUrinary(model=> {
//      console.log(model,'oobj');})
//       resolve(model)
//
//   });
//   // obj.urinarySystem = generateUrinary()
//   // obj.digestive = generateDigistive()
//   // return obj;
//   let result = await promise; // wait till the promise resolves (*)
//   obj.urinarySystem = result
//   return obj;
//
// }









// const json = require('big-json');
//
//
// const stringifyStream = json.createStringifyStream({
//     body: BIG_POJO
// });
//
// stringifyStream.on('data', function(strChunk) {
//     // => BIG_POJO will be sent out in JSON chunks as the object is traversed
// });


function request(object) {
  console.log('hello request');
  fetch('/urinarySystem', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "credentials": "same-origin"
      },
      body: JSON.stringify(object)
    }).then(response => {
      return response.json()
    })
    .then(res => {
      console.log(res);
    })
    .catch(error => {
      console.error(`Fetch Error =\n`, error)
      console.log(error);
    });
}
