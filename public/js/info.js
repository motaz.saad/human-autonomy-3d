function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', '/js/info.json', true); // Replace 'my_data' with the path to your file
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == "200") {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}


function init(data) {
  loadJSON(response => {
    // Parse JSON string into object
    var actual_JSON = JSON.parse(response);
    data(actual_JSON)
  });
}


var i;
var j;
var speed = 50;
var list = document.getElementById('list')
var timeout;

function getSystemName(systemName, member) {
  clearTimeout(timeout);
  i = 0
  j = 0
  init(textArray => {
    function typeWriter() {
      var title = textArray[systemName].title,
        info = textArray[systemName].info;
      if (member && member != undefined) {
        document.getElementById("h1").innerHTML = title
        for (j = 0; j < info.length; j++) {
          var item = document.createElement('li')
          var desc = document.createElement('ul')
          var h4 = document.createElement('h3')
          if (Object.keys(info[j])[0] === member) {
            h4.innerHTML = member;
            item.appendChild(h4)
            var descArr = Object.values(info[j])[0]
            for (m = 0; m < descArr.length; i++) {
              var descItem = document.createElement('li')
              descItem.setAttribute('class', 'text')
              descItem.style.fontSize = '18px'
              descItem.innerHTML = descArr[m]
              desc.style.lineHeight = '191%'
              desc.appendChild(descItem)
              m++
            }
            item.appendChild(desc)
            document.getElementById('list').innerHTML = ''
            document.getElementById('list').setAttribute('class', 'animated fadeInRight list')
            document.getElementById('list').style.background = 'black'
            document.getElementById('list').appendChild(item)
            break;
          }
        }
      } else {
        document.getElementById("list").innerHTML = ''
        document.getElementById('list').style.background = 'none'
        if (document.getElementById("h1").innerHTML != title) {
          if (i < title.length) {
            document.getElementById("h1").innerHTML += title.charAt(i);
            i++;
            timeout = setTimeout(typeWriter, speed);

          }
        }
      }

    }
    typeWriter()
  })

}
