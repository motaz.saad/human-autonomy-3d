# human-autonomy-3d

## Demo URL

1. http://mksaad.tk:8080/
2. https://ishrahly.herokuapp.com

 ### Run This Project In Local Machine: 
 
 

1.  Clone the repo:

    ## SSH:

        ```
        git@gitlab.com:motaz.saad/human-autonomy-3d.git
        ```
        
    
    ## HTTPS:
    
        ```
        https://gitlab.com/motaz.saad/human-autonomy-3d.git
        ```
2.  Install the node.js devDependencies:
    ```
    npm install
    ```
    OR 
    ```
    npm i
    ```
3.  Run the Server:
    ```
    npm start
    ```
    OR 
    ```
    npm run start
    ```
    
### To Edit Port: 

1. Open index.js file from this root: 

    ```
    src/index.js
    ```
2. Change Port from line 8
    ```
    const PORT = process.env.PORT || 8080;
    ```

### herokuapp deploy steps 

https://devcenter.heroku.com/categories/nodejs-support

```
snap install heroku --classic
heroku login
node --version
npm --version
```

#### for the first time 
```
heroku create
```

#### to update deployment using a fresh machine 

```
heroku git:remote -a ishrahly
git clone https://gitlab.com/motaz.saad/human-autonomy-3d.git
cd human-autonomy-3d 
git push heroku master
```

#### to update deployment using previous clone  

```
cd human-autonomy-3d 
git pull
git push heroku master
```



#### Remote heroku repo :

https://git.heroku.com/ishrahly.git

### Deploy on Linux VPS 
```
npm install pm2 -g
cd human-autonomy-3d
pm2 start src/index.js
pm2 list 
pm2 restart 0
```

### Server URL 
http://mksaad.tk:8080/
    
