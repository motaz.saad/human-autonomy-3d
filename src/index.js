const path = require('path');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const routes = require('./controllers/routes');

const PORT = process.env.PORT || 8080;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ limit: '50mb',extended: false }));
app.use('/', routes);
app.use(function(req, res, next) {
  // check  extension
  console.log(req.originalUrl,'url');
  next();
}, express.static(__dirname));
app.use(express.static(path.join(__dirname, '../public')));
app.use('/*', express.static(path.join(__dirname,'..','public','index.html')));

app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
});
